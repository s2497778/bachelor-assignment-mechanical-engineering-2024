\section{Analysis}

\subsection{Mapping of the system}
Using robotic systems in large scale western production systems requires a significant change in the configuration of the socio-technical system. This section will map out the system before and after the introduction of robots within these factories. This will provide a comprehensive view of how technology transforms the landscape of production facility.

\subsubsection{Configuration Before Robot Introduction}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=0.7\textwidth]{Figures/Socio-technical-before.png}
    \caption{Socio-technical configuration before fully automated production line}
    \label{fig:Socio-before}
\end{figure*}

Figure \ref{fig:Socio-before} shows how the socio-technical configuration of a production facility looks like before the introduction of a fully automated production line. The main principle in this configuration is that a lot of work is done by the human operator. Relying on human labor is of course more expensive than using a robot in most cases, but it has the advantage of being more flexible in all kinds of ways. This could be for example in the way the work is done; the human operator can easily adapt to new situations, while a robot needs to be reprogrammed. Next to this the way the work is planned can also be changed more easy, by using for a time period more or less human operators. \cite{Tilley2017}

The industry structure in this configuration is characterized by humans performing most of the tasks in the production processes. This is done by using traditional assembly lines, where human operators are responsible for the production of goods. This means that the worker is responsible for the quality of the product that the company produces. This could be favorable in some cases, for example when working with materials coming from nature. These materials can have a lot of variation in quality, and a human operator can easily adapt to this. \cite{assembly_line_blog}

Market practices are characterized by a skilled labor market, where worker productivity drives the output. Skilled labor is in most cases needed as it influences the productivity of the output of the production line. This can be both in volume as well as in quality.

Regulations and policies in the human focused configuration are based on human safety- and working rights. In these working rights are for example the maximum amount of hours that a worker can work in a week, but also the minimum amount of breaks that a worker should have. Next to this there are also regulations on the safety of the worker, for example the maximum amount of noise that a worker can be exposed to or that the company should provide human workers with personal protection. \cite{OHCHR2019}

Infrastructure in such factories should be designed for human workers. This is why facilities and equipment should be tailored to human capabilities. Basic IT systems are used primarily for production monitoring, keeping track of the production process. This is done by for example counting the amount of products that are produced, but also the amount of products that are rejected.

Maintenance of machinery is handled by human technicians who have the expertise to troubleshoot and repair equipment. This is done by for example checking the oil level of a machine, but also by checking the wear of the machine. This is done to prevent the machine from breaking down, which could lead to a stop in the production process.

Lastly, the cultural aspects of the factory emphasize craftsmanship and teamwork, valuing the skills and collaborative efforts of human workers. This is done by for example giving a bonus to the team that has the highest production rate, but also by giving a bonus to the worker that has the highest quality of the products that are produced. \cite{StanfordGSB2019}

Overall, the socio-technical system before the introduction of robotic systems relies heavily on human labor, offering flexibility and adaptability. Skilled labor drives productivity, with regulations ensuring worker safety, and the cultural environment valued craftsmanship and teamwork.



\subsubsection{Configuration After Robot Introduction}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=0.7\textwidth]{Figures/Socio-technical-after.png}
    \caption{Socio-technical configuration after a fully automated production line}
    \label{fig:Socio-after}
\end{figure*}

Figure \ref{fig:Socio-after} shows the socio-technical configuration of a factory after the integration of robotic automation. In this industry structure, a lot, if not all of the tasks that were done by humans on the production line are now done by robots using fully automated production lines.

Market practices in this configuration change to needing tech savvy workers that are responsible for the robot and its workings as opposed to making a product themselves. One could see that here less workers are needed at the factory as one person can handle multiple robots. This would then lead to a decrease in the amount of workers that are needed at the factory. 

Regulations and policies in this configuration should be based on robot safety and data protection. This is because robots could danger the safety of the humans working with them. Next to this, these robots will produce a lot of data, this should be protected as it could contain sensitive information. Next to this robots are sensitive for hacking which could lead to a complete stop in the production process. \cite{NIOSH2021}

Infrastructure changes from human- to robot-centric, meaning that the facilities and equipment should be designed for robots. This could not only mean a difference in the way the production line is set up, but also in the way the factory itself is built. For example, the factory should be built in such a way that the robots can easily move around, but also that the robots can be easily maintained. The information technology systems will also change a lot as the robots will produce a lot of data that should be stored and analyzed in order to advance their AI capabilities. Next to this the robots should be able to communicate with each other. \cite{Tantawi2024}

Maintenance of machinery is still needed, but the role of the human technician evolves to focus more on overseeing and maintaining advanced robotic systems instead of more easy machinery. This is done by for example checking the data that the robot produces, but also by checking the wear of the robot. This is done to prevent the robot from breaking down, which could lead to a stop in the production process.

The cultural aspect of the factory changes to valuing efficiency, and precision more then before. This is because the robots are able to produce a lot more products in a shorter amount of time, but also with a higher quality standards. This could lead to a higher output of the factory, but also to a higher quality of the products that are produced.

In summary, the socio-technical system after the introduction of robots is technology-centric. This means practically that the factory requires fewer, more tech-savvy workers. Regulations focus on robot safety and data protection, while infrastructure is redesigned for robot efficiency. The cultural emphasis moves to valuing efficiency and precision, resulting in higher output and quality.


\subsection{Actor analysis}

The transition to utilizing robotic systems in a production factory production involves an array of stakeholders, each contributing to and being affected by this shift in various ways. This actor analysis categorizes these stakeholders into primary, secondary, and external actors. It will explore their roles, interactions, and the potential impacts of increased automation. Figure \ref{fig:Actor} visually represents this analysis using a value chain framework.

\subsubsection{Primary Actors}

Primary actors are directly engaged in the production process. Four different primary actors are identified: inbound logistics, operations, outbound logistics, and customers. Each of these actors plays a critical role in the production process and is affected by the introduction of robotic systems.

Inbound logistics is responsible for sourcing and receiving raw materials and parts necessary for production. If robotic systems are introduced, this actor must adapt its process in order to align with the newly introduced systems. This could mean adopting new inventory management technologies and workflows. The need for synchronization between the logistics processes and the automated systems is crucial to ensure a smooth integration of the robots with the inbound logistics. \cite{inboundlogistics2021}

In operations, the production process actually happens. This is where robotics are used to produce the goods that were previously produced by human workers. This actor is already previously discussed in the socio-technical configuration.

Outbound logistics can be as the opposite of inbound logistics, the processes look very much the same but now the goods are being transported to the customers. This actor is influenced by the introduction of robotic systems as well. As the error rate of the robots is lower than that of the human workers, the outbound logistics can be more efficient. This could lead to a decrease in the amount of workers that are needed in this process. \cite{inboundlogistics2021}

Customers play a crucial role in driving the adoption of robotic technology. Their expectations for high-quality products and cost-efficiency push manufacturers to adopt advanced automation to meet these demands. Enhanced product quality and reduced production costs resulting from automation can lead to higher customer satisfaction and loyalty.

\subsubsection{Secondary Actors}

Secondary actors support the primary production activities. Two distinct actors are identified and include companies that produce robots and the groups that research and implement them (R\&D).

The robots themselves can be seen as a secondary actor as they are responsible for the automation of the process. Different suppliers and companies produce these robots. Together they are partly responsible for the processes occurring in the operations department.

Research and Development (R\&D) is responsible for the development of new technologies inside the company. They can introduce these robotics, write specifications for them, and implement them in the production process. They can also be responsible for optimizing different production steps taken or implementing new production routines. This is done to ensure that the robots are becoming more efficient and reliable. \cite{mckinsey_2020}


\subsubsection{External Actors}

External actors influence the production process from outside the immediate factory environment. Six different external actors are identified: regulators, labor unions, investors, industry associations, competitors, and educational institutions. Each of these actors plays a different critical role in the transition to automated manufacturing.

Regulators set the legal and safety standards that factories must adhere to. The introduction of robots introduces new regulatory challenges, including safety protocols for human-robot interactions and data protection laws to safeguard sensitive information. \cite{Tantawi2024}

Labor unions represent the interests of workers, negotiating the impacts of automation on employment and working conditions. As robots take over routine tasks, unions play a vital role in advocating for retraining programs and fair treatment of displaced workers. \cite{gorg2009}

Investors provide the financial resources necessary for implementing robotic systems. Their decisions will significantly influence the pace and extent of automation adoption. A supportive investment climate can accelerate the transition to automated manufacturing, while cautious or reluctant investors may slow down the process.

Industry associations offer a platform for industry-wide collaboration and standard-setting. They facilitate the sharing of best practices and support companies in navigating the transition to automation. These associations can help standardize new technologies and promote innovation across the industry. \cite{WATKINS20151407}

Educational institutions are responsible for training the future workforce. As the demand for skills related to robotics and automation grows, these institutions must adapt their curricula to prepare students for the evolving job market. Ensuring a steady supply of qualified personnel is essential for sustaining the advancements in automated manufacturing. \cite{Leticia:2023}

Competitors will influence market dynamics and competitiveness. The adoption of robotics by leading companies can drive others to follow in order to maintain their competitive edge.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.48\textwidth]{Figures/Actor_analysis.drawio.png}
    \caption{Actor analysis}
    \label{fig:Actor}
\end{figure}
