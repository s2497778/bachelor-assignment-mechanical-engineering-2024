\LoadClass[twocolumn,12pt]{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ARandS}[2024/04/18 by B.M. de Gooijer]

% Uses geometry to set the margins
\RequirePackage[a4paper,top=20mm,bottom=20mm,right=12mm,left=12mm]{geometry}
\RequirePackage{ragged2e}
\usepackage{flushend} %equal column length on last page
%\raggedbottom 

% Load graphics packages
\RequirePackage{graphicx,tikz,pgfplots}
\pgfplotsset{compat=newest}
\pgfplotsset{every axis/.append style={
                    label style={font=\footnotesize},
                    tick label style={font=\footnotesize}  
                    }}
\usepackage{forest}
\usetikzlibrary{trees}
\usepackage{adjustbox}
\usetikzlibrary{shapes.geometric, arrows.meta}

% Use hyperref to get clickable hyperlinks
\PassOptionsToPackage{hyphens}{url}
\RequirePackage[hidelinks]{hyperref}


% Set the column seperation on 0.7 cm
\columnsep 7mm
\RequirePackage{amsmath} %used to be flushed left [fleqn]

% Set the indents to zero
%\setlength\parindent{\z@}
%\setlength\mathindent{\z@} %removed for centered equations

% Set Times Roman as default Serif font
\renewcommand{\rmdefault}{ptm} % roman (serif)

% Define new commmand "\affiliation"
\def\affiliation#1{\gdef\@affiliation{#1}}
\def\@affiliation{\@latex@warning@no@line{No \noexpand\affiliation given}}

% Define new commmand "\abstract"
\def\abstract#1{\gdef\@abstract{#1}}
\def\@abstract{\@latex@warning@no@line{No \noexpand\abstract given}}

% Define new commmand "\keywords"
\def\keywords#1{\gdef\@keywords{#1}}
\def\@keywords{\@latex@warning@no@line{No \noexpand\keywords given}}

% Define new command "\acknowledgements{}" which is not obligatory
\long\def\acknowledgements#1{\smallsection*{Acknowledgements}{\fontsize{10}{10}\selectfont
#1\par % paragprah should be ended, otherwise it fs up the line spacing
}}
%\ignorespaces


% Define new command "\availabilityofdata{}" which is not obligatory
\long\def\availabilityofdata#1{\smallsection*{Availability of data and materials}{\fontsize{10}{10}\selectfont 
#1\par % paragprah should be ended, otherwise it fs up the line spacing
}}

% Define command \maketitle to print the title, author and affilation
\def\@maketitle{%
  \newpage
  \null
  \begin{center}%
    {\fontsize{18}{18}\selectfont \@title \par} % Prints title in 18 pt normal font
    \vskip 18pt
    {\fontsize{14}{12}\selectfont \@author \par} % Prints author in 14 pt normal font
    \vskip 12pt
    {\fontsize{12}{12}\itshape \selectfont \@affiliation \par} % Prints affiliation in 12 pt italic font
  \end{center}%
}
  
% Define command \makeabstract to print the abstract and the key words
\def\makeabstract{%
	\begin{center}%
	{\justify \fontsize{12}{12}\selectfont ABSTRACT: \@abstract \par} % Prints abstract after "ABSTRACT:" in 12 pt normal font, fully justified
	\vskip 12pt
	{\flushleft\fontsize{12}{12}\selectfont Key words: \@keywords \par} % Prints key words after "Key words:" in 12 pt normal font
	\vskip 12pt
	\end{center}
}

% Set the numbering of subsubsections to alphanumeric (a, b, c) 
\renewcommand\thesubsubsection{\thesubsection.\@alph\c@subsubsection}

% Set the fonts of the section headings
\def\section{\@startsection{section}{1}{\z@}{24pt plus \z@ minus \z@} % Section is 12 pt capital letters normal font
{12pt plus \z@ minus \z@}{\fontsize{12}{12}\selectfont\bfseries\MakeUppercase}}%

\def\smallsection{\@startsection{section}{1}{\z@}{20pt plus \z@ minus \z@} % Smallsection is 10 pt capital letters normal font
{10pt plus \z@ minus \z@}{\fontsize{10}{10}\selectfont\MakeUppercase}}%

\def\subsection{\@startsection{subsection}{2}{\z@}{12pt plus \z@ minus \z@} % Subsection is 12 pt italic font
{18pt plus \z@ minus \z@}{\fontsize{12}{12}\selectfont\bfseries}}%

\def\subsubsection{\@startsection{subsubsection}{3}{\z@}{6pt plus \z@ minus \z@} % Subsubsection is 12 pt normal font
{1sp plus \z@ minus \z@}{\fontsize{12}{12}\selectfont\textit}}%

% CAPTIONS
\RequirePackage[font=footnotesize]{subcaption}

% Define a string to compare something with
\def\@tablestring{table}

% Define the caption make command
\long\def\@makecaption#1#2{
	\ifx\@captype\@tablestring
		% If the caption is of a table, use the following parameters:
		\setlength\abovecaptionskip{10\p@}
		\setlength\belowcaptionskip{\z@}
		\def\capflush{\flushleft}
		\def\caphfill{}
	\else
		% Else, the caption is of a figure, use the following parameters:
		\setlength\abovecaptionskip{6\p@}
		\setlength\belowcaptionskip{6\p@}
		\def\capflush{}
		\def\caphfill{\hfil}
	\fi
  \vskip\abovecaptionskip
  \sbox\@tempboxa{\fontsize{10}{10}\selectfont #1: #2}%
  \ifdim \wd\@tempboxa >\hsize
  	% If caption is longer than a line, do this:
    {\capflush\fontsize{10}{10}\selectfont #1: #2}\par
  \else
  	% Else the caption fits on a line, do this:
    \global \@minipagefalse
    \hb@xt@\hsize{\caphfill\box\@tempboxa\hfil}%
  \fi
\vskip\belowcaptionskip}

% Change how figures and tables are called in captions
%\renewcommand\figurename{Fig.}
\renewcommand\tablename{Table}

% Turn off page numbering entirely
\pagenumbering{gobble}

% (Re)define thebibliography environment so that references use small font in title; rest is untouched because I don't want to break that.
\renewenvironment{thebibliography}[1]
     {\fontsize{10}{10}\selectfont
     \smallsection*{\refname}%
           \@mkboth{\MakeUppercase\refname}{\MakeUppercase\refname}%
                \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{\fontsize{10}{10}\selectfont #1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}
      \let\@openbib@code\@empty

% Enable the use of appendices
\RequirePackage[title]{appendix}%
\renewcommand\appendixname{Appendix}%


\RequirePackage{etoolbox}
% Inserts \clearpage before \begin{appendices}
\BeforeBeginEnvironment{appendices}{\clearpage}
% New command to insert AI statement
\newcommand{\aistatement}[1]{\patchcmd{\thebibliography}{\list}{#1\list}{}{}}

\renewenvironment{quote}
{\vspace{0.0em}\list{}{\rightmargin\leftmargin}%
 \item\relax\small\vspace{-0.5em}} % Adjust -0.5em to the desired negative value
{\endlist\vspace{-0.5em}}


\RequirePackage{enumitem}
\setlist[itemize]{itemsep=5pt, topsep=0pt, partopsep=1pt, parsep=1pt}
\setlist[enumerate]{itemsep=4pt, topsep=0pt, partopsep=1pt, parsep=1pt}


\usepackage{float}
%%%%%%%%%%%%%% APPPENDICES
% \@ifpackageloaded{appendix}{%
% \AtBeginDocument{%
% %
% \let\oldappendices\appendices%
% \let\oldendappendices\endappendices%
% %
% \renewenvironment{appendices}{%
% \setcounter{figure}{0}%
% \setcounter{table}{0}%
% \setcounter{equation}{0}%
%     \if@twocolumn %ensure Appendix is in onecolumn format
%         \onecolumn
%     \fi
% %%
% \begin{oldappendices}%
%   \gdef\thefigure{\@Alph\c@section\arabic{figure}}%
%   \gdef\thetable{\@Alph\c@section\arabic{table}}%
%   \gdef\theequation{\@Alph\c@section\arabic{equation}}%
% }{\end{oldappendices}}
% }
% %%
% }{}  

% APPENDICES EXAMPLE IN MAIN%
% \begin{appendices}
% \section{}\label{app:A}

% \begin{table*}[h!] 
%     \centering
%     \caption{Different Fraction of Variance Unexplained based on the validation set using the model with direct interpolation versus the truncated models with $K$ basis vectors when $R_K<0.1$. Best performing model for that training set is given in italics.}
%     \label{tab:FVU_K}
%     \begin{tabular}{c r c c c c c}
%                         & $N_\mathrm{set}=$ &  1  & 2 & 3 & 4 & 5 \\
%         \hline
%         \rule{0pt}{1.1\normalbaselineskip}
% {$N_\mathrm{exp} = 20$}   
%                         & Direct, $\delta$ & 1.63$\cdot10^{-2}$ & 1.87$\cdot10^{-2}$ & {\it1.67$\cdot10^{-2}$} & 2.22$\cdot10^{-2}$ & 2.15$\cdot10^{-2}$ \\ 
%                         & SVD+array, $\tau$ & {\it 1.53$\cdot10^{-2}$ } &  {\it1.71$\cdot10^{-2}$} & 1.69$\cdot10^{-2}$ & {\it2.21$\cdot10^{-2}$} & {\it2.09$\cdot10^{-2}$} \\ 
%                         & $K$ & 10 & 11 & 10 & 10 & 10 \\ 
%                         & SVD+scalar, $\tau$ & 2.30$\cdot10^{-2}$ & 2.31$\cdot10^{-2}$ & 1.84$\cdot10^{-2}$ & 3.10$\cdot10^{-2}$ & 2.39$\cdot10^{-2}$ \\
%                         & $K$ & 12 & 13 & 11 & 11 & 11 \\ 
%         \hline
%         \rule{0pt}{1.1\normalbaselineskip}
% {$N_\mathrm{exp} = 40$}   
%                         & Direct, $\delta$ & {\it7.49$\cdot10^{-3}$} & {\it9.15$\cdot10^{-3}$} & {\it8.17$\cdot10^{-3}$} & {\it8.31$\cdot10^{-3}$} & {\it8.69$\cdot10^{-3}$} \\  
%                         & SVD+array, $\tau$ & 7.57$\cdot10^{-3}$ & 9.18$\cdot10^{-3}$ & 8.52$\cdot10^{-3}$ & 8.43$\cdot10^{-3}$ & 9.37$\cdot10^{-3}$ \\ 
%                         & $K$ & 16 & 18 & 16 & 20 & 18 \\ 
%                         & SVD+scalar, $\tau$ & 1.11$\cdot10^{-2}$ & 1.10$\cdot10^{-2}$ & 1.21$\cdot10^{-2}$ & 1.00$\cdot10^{-2}$ & 9.68$\cdot10^{-3}$ \\ 
%                         & $K$ & 17 & 19 & 19 & 21 & 19 \\ 
%         \hline
%         \rule{0pt}{1.1\normalbaselineskip}
% {$N_\mathrm{exp} = 60$}   
%                         & Direct, $\delta$ & 5.45$\cdot10^{-3}$ & 6.07$\cdot10^{-3}$ & 5.38$\cdot10^{-3}$ & 5.40$\cdot10^{-3}$ & 5.59$\cdot10^{-3}$ \\ 
%                         & SVD+array, $\tau$ & {\it5.40$\cdot10^{-3}$} & {\it5.94$\cdot10^{-3}$} & {\it5.34$\cdot10^{-3}$} & {\it5.27$\cdot10^{-3}$} & {\it5.56$\cdot10^{-3}$} \\ 
%                         & $K$ & 24 & 23 & 24 & 24 & 23 \\ 
%                         & SVD+scalar, $\tau$ & 7.08$\cdot10^{-3}$ & 8.55$\cdot10^{-3}$ & 5.42$\cdot10^{-3}$ & 6.68$\cdot10^{-3}$ & 6.97$\cdot10^{-3}$ \\
%                         & $K$ & 25 & 25 & 26 & 26 & 26 \\  
%         \hline
%         \rule{0pt}{1.1\normalbaselineskip}
% {$N_\mathrm{exp} = 80$}   
%                         & Direct, $\delta$ & {\it4.57$\cdot10^{-3}$} & 4.40$\cdot10^{-3}$ & {\it4.33$\cdot10^{-3}$} & 4.10$\cdot10^{-3}$ & 3.76$\cdot10^{-3}$\\  
%                         & SVD+array, $\tau$ & 4.65$\cdot10^{-3}$ & {\it4.34$\cdot10^{-3}$} & 4.35$\cdot10^{-3}$ & {\it4.06$\cdot10^{-3}$} & {\it3.73$\cdot10^{-3}$}\\
%                         & $K$ & 29 & 29 & 28 & 31 & 30 \\ 
%                         & SVD+scalar, $\tau$ & 5.33$\cdot10^{-3}$ & 4.80$\cdot10^{-3}$ & 4.72$\cdot10^{-3}$ & 4.46$\cdot10^{-3}$ & 3.88$\cdot10^{-3}$\\
%                         & $K$ & 30 & 31 & 30 & 33 & 33 \\ 
%     \end{tabular}
% \end{table*}

% \end{appendices}



      
% % Change the spacing between bibliography entries in a very sick manner
% \let\oldbibliography\thebibliography
% \renewcommand{\thebibliography}[1]{%
% \oldbibliography{#1}%
% \setlength{\itemsep}{0pt}%
% }

%%%%%%%%%%%%%%%
% (Re)define \thebibliography command so that references use small font in title; rest is untouched because I don't want to break that.
% \def\thebibliography#1{
% \fontsize{10}{10}\selectfont \smallsection*{\refname}%
% 	\addcontentsline{toc}{section}{\refname}%
%     % V1.6 add some rubber space here and provide a command trigger
%     \list{\@biblabel{\@arabic\c@enumiv}}%
%     {\settowidth\labelwidth{\@biblabel{\fontsize{10}{10}\selectfont #1}}%
%     \leftmargin\labelwidth
%     \advance\leftmargin\labelsep\relax
%     \itemsep
%     \usecounter{enumiv}%
%     \let\p@enumiv\@empty
% 	\let\@IEEElatexbibitem\bibitem%
%     \def\bibitem{\@IEEEbibitemprefix\@IEEElatexbibitem}}}

% For example, the following text is \textbf{\underline{not}} allowed: ``...and if we consider that $a=\frac{c}{d}$ and $b=\frac{1-c}{d}$ then we obtain $a+b=\frac{1}{d}$ so that..."

% This text must be rewritten in several paragraphs (using the equation editor) as follows: 
% ``...and if we consider that:
% \begin{equation}
%     a =\frac{c}{d} \quad\text{and}\quad b = \frac{1-c}{d} 
% \end{equation}
% then we obtain:
% \begin{equation}
%     a+b=\frac{1}{d}
% \end{equation}
% so that..."

% Or in the plain text as follows: `` ...and if we consider that $a=\frac{c}{d}$ and $b=(1-c)$ then we obtain $a+b=1/d$ so that..."