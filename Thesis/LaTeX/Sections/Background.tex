\section{Background}

ROS can be used to simulate and control robots in a virtual environment. It is a middleware that provides a framework for developing robot specific software and routines. Currently two versions of ROS are available and supported, mainly ROS1 and ROS2. The difference between the two is quite significant. ROS2 has a different philosophy on how to handle communication between nodes, real time systems and multiple devices. 

ROS1 is the first version of ROS and is still being used widely to this day, however its end of life date is moving fast as it is currently set at May 2025. Its architecture is based on a single master node that handles all communication between nodes. This master node is responsible for keeping track of all nodes and their services. This also means that the master node is critical for operation, it makes sure that all nodes can discover each other. The architecture of ROS1 is shown in figure \ref{fig:ROS1}. Three different types of communication are supported in ROS1, namely service, action and topic. A service is a request-response communication, where a node sends a request to another node and waits for a response. An action is similar to a service, but it can send multiple responses and is based on a goal. A topic is a one way communication, where a node can publish data to a topic and other nodes can subscribe to this topic to receive the data. Devices in ROS1 can talk to each other over UPD or TCP, these protocol are mainly used in networks. This means in practice that ROS1 is not real time, as it can not guarantee that a message will be received in a certain time frame. This is due to the fact that the underlying communication protocol, TCP, does not guarantee that a message will be received in a certain time frame.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{Figures/ROS1.png}
    \caption{ROS1 architecture}
    \label{fig:ROS1}
\end{figure}

ROS2 is the second version of ROS and is meant to be an improvement over ROS1. It is designed to be more modular, distributed and real-time. The architecture of ROS2 is shown in figure \ref{fig:ROS2}. The main improvements of ROS2 is that it is designed to be real time and does not require a master node. It still uses the same communication types as ROS1, namely service, action and topic, however these can be extended by the user if they have a specific requirement for a communication type. All nodes can discover each other simultaneously without the need of a masternode coordinating this. Different machines can communicate with each other over DDS (Data Distribution Service), this protocol is designed to be real time and can guarantee that a message will be received in a certain time frame. It is an industry standard for real time systems, integration with different middleware or other systems. Under the hood, ROS2 is also designed to be more modular, meaning easier integration of new features such as using other programming languages in nodes.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{Figures/ROS2.png}
    \caption{ROS2 architecture}
    \label{fig:ROS2}
\end{figure}

In figure \ref{fig:compare_versions} it can be seen how ROS1 and ROS2 differ in a lower level architecture. The greyed area is the part that ROS is responsible for. It can be seen that ROS1 is responsible for more sub-systems. ROS1 is responsible for the master node, the communications between nodes using rclcpp, rclpy, rcl and the communication between the different systems using UDP/TCP and between the software and the operating system using a system Application Programming Interface (API). Comparing this to ROS2, which is only responsible for the communication between nodes using rclcpp, rclpy, rcl and the communication between the operating system and the middleware using a system API. This means that ROS2 is more modular and in most use cases faster than ROS1.

rclcpp and rclpy\footnote{Note that these packages have the same name in ROS1 and ROS2, but are in fact different packages that work in a different manner.} are respectively the C++ and Python client libraries for ROS. These libraries are responsible for the communication between nodes. rcl is the ROS client library, which is responsible for the communication between the different systems. Other programming languages can interact with this library using a similar system as rclcpp and rclpy but these need to be written by the user, as they are not included with the core of the ROS system. The system API is used to communicate between the software and the operating system used on the device.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{Figures/ROS1vsROS2.png}
    \caption{ROS versions compared}
    \label{fig:compare_versions}
\end{figure}

ROS1 and ROS2 can be used together, using ROS bridge. This allows for communication between nodes in ROS1 and ROS2. The bridge is meant for transitioning from ROS1 to ROS2 for robots who do not support ROS2 yet. The bridge is not meant for long time permanent use, as it is not as efficient as using ROS2 natively. It works by creating an additional node between the two systems as can be seen in figure \ref{fig:bridge}. This node translates messages and services between these nodes. It comes at the cost of slower speeds and higher latency. This is due to the fact that CPU cycles need to be used to translate messages between the two systems. If a lot of data is passed though this bridge, it can become a bottleneck and slow down the system by dropping data. Next to this a major disadvantage is that the bridge does not support all message types and services. If a user want to use their own message type, the bride needs to be rewritten to support this. Finally the bridge currently only supports C++ nodes, meaning that the node needs to be compiled before it can be used, and thus can not be changed on the fly.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{Figures/Bridge.png}
    \caption{ROS bridge}
    \label{fig:bridge}
\end{figure}
